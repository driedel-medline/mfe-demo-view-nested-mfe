const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

module.exports = {
  output: {
    uniqueName: "mfe2"
  },
  optimization: {
    // Only needed to bypass a temporary bug
    runtimeChunk: false
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "mfe2",
      filename: "remoteEntry.js",
      remotes: {
        masterMessage: "masterMessage@http://localhost:2200/remoteEntry.js",
      },
      exposes: {
        './Component': './src/app/app.component.ts',
        './Module': './src/app/bookings/bookings.module.ts'
      },
      shared: {
        "@angular/core": { singleton: true },
        "@angular/common": { singleton: true },
        "@angular/router": { singleton: true },
        "medline-mfe-store": { singleton: true }
      }
    }),
  ],
};
